import React, { Component } from 'react';
import logo from './logo.svg';
import { apiGet } from './utils/api';
import './App.css';

const Row = item => (<li>
  <p>
    <b>Name:</b> {item.data.username} <b>Email:</b> <a href={`mailto: ${item.data.email}`}>{item.data.email}</a>
  </p>
</li>);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentWillMount() {
    apiGet('/api/users').then((data) => {
      console.log(data);
      this.setState({ data });
    });
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React & Django</h2>
        </div>
        <div>
          <ol>
            {this.state.data.map((item, index) => <Row key={index} data={item} />)}
          </ol>
        </div>
      </div>
    );
  }
}

export default App;
