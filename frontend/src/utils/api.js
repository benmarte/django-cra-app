export const apiGet = (endpoint) => {
  return fetch(`http://localhost:8000${endpoint}`, {
    method: "get",
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
  }).then(function(data) {
      return data.json();
  }).then(function(data) {
      return data;
  }).catch(function(ex) {
      return ex;
  });
}