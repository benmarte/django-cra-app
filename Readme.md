# Django Create React App

Is a starter app project which uses Python, Django with Django Rest Framework (DRF) as the apps backend and api and uses a Create React App (CRA) front end for the UI.

## Table Of Contents

- [Requirements](#requirements)
- [Project Structure](#project-structure)
- [Run Migrations](#run-migrations)
- [Create Super User](#create-super-user)
- [Run The App](#run-the-app)
- [Restart The App](#restart-the-app)
- [Log In to the Django Admin Interface](#log-in-to-the-django-admin-interface)
- [Log In to Django Rest Framework](#log-in-to-django-rest-framework)
- [Docker Container Names](#docker-container-names)
- [Persisting Data in Windows](#persisting-data-in-windows)

## Requirements

- [Docker](https://www.docker.com/community-edition#/download) installed locally
- The code files in this repo

## Project Structure

This project consists of 2 folders

- backend: contains the files to run the python, django and postgres containers to run the backend and api
- frontend: contains all the files to run the react UI of the application

## Run Migrations

Run database migrations

- `docker-compose run api python manage.py migrate`

## Create Super User

Create the admin user

- `docker-compose run api python manage.py createsuperuser`

## Run The App

- `docker-compose up`

## Restart The App

If you need to restart the app for some reason

- `docker-compose restart`

## Log In to the Django Admin Interface

To log in to the Django Admin interface once the containers are all running go to: `http://localhost:8000/admin` and you can log in using the user you created.

## Log In to Django Rest Framework

To log into the Django Rest Framework you can do so by going to: `http://localhost:8000/api` and log in with your users credentials.

## Docker Container Names

If you need to access one of the Docker containers while they are running you can access them via the conatainer names, these are as follows:

- Python/Django Container: `app_api`
- Postgres Database Container: `app_db`
- Create React App Container: `app_ui`
 
## Persisting Data In Windows

If you would like to persist data when running this app in a Windows environment you will need to create a docker volume in order to achieve this since postgres linux file system does not work well in a windows file system.
[Here is an example on how to achieve this](https://forums.docker.com/t/trying-to-get-postgres-to-work-on-persistent-windows-mount-two-issues/12456/5).

If you are not concerned with persisting data then you can remove lines `6` and `7` from the `docker-compose.yml` file, as long as you don't delete your container you will have access to your data.